stages:
  - build
  - build_second
  - build_third
  - build_doc
  - test
  - deploy

##################### Common CI settings ##############################

.CI-common:
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests@sdurobotics/RobWork
      - /^ci-.*$/
  except:
    refs:
      - debian-pkg@sdurobotics/RobWork
      - /^wip-.*$/
    variables:
      - $CI_COMMIT_MESSAGE =~ /^WIP:/

########################## Linux CI ####################################
.alisases:
  - &is_shared_runner
    - "[[ $CI_RUNNER_DESCRIPTION =~ 'shared-runners-manager' ]] && SHARED_RUNNER='true' || SHARED_RUNNER=''"
  - &remove-rw-install
    - shopt -s extglob
    - rm -r -v /usr/local/bin/!(ccmake|cmake|cpack|ctest|ode-config) || echo 
    - rm -r -v "/usr/local/lib/!(python2.7|python3.6)" || echo 
    - rm -r -v /usr/local/include/!(ode) || echo 
    - rm -r -v /usr/local/share/!(ca-certificates|fonts|man|applications|info) || echo 
  - &uninstall-rw-deb
    - apt -y remove *sdurw* || echo "no pkg to remove"
    - apt -y remove robworkstudio* || echo "no pkg to remove"
    - apt -y autoremove || echo "no pkg to remove"

.CI-common-linux:
  extends: .CI-common
  before_script:
    - *remove-rw-install
    - THREADS=$(nproc --all)
    - WERROR=True

.linux-doc:
  extends: .CI-common
  stage: build_doc
  tags:
    - linux
  before_script:
    - "[ -d Build ] || mkdir Build"
    - "[ -d Build/RW ] || mkdir Build/RW"
    - "[ -d Build/sphinx ] || mkdir Build/sphinx"
  script:
    - cd Build
    # Build Sphinx
    - cd sphinx
    - cmake -DCMAKE_BUILD_TYPE=Release -DRobWork_DIR:PATH=../../RobWork/cmake -DRobWorkStudio_DIR:PATH=../../RobWorkStudio/cmake -DRobWorkSim_DIR:PATH=../../RobWorkSim/cmake ../../doc/sphinx
    - make sphinxapidoc -j$(nproc --all) || make sphinxapidoc -j$(nproc --all)
    # Publish
    - cd ../..
    - mv Build/sphinx/html public
  artifacts:
    paths:
      - public
    expire_in: 1 day

.linux-build-RW:
  extends: .CI-common-linux
  stage: build
  tags:
    - linux
  before_script:
    - *remove-rw-install
    - THREADS=$(nproc --all)
    - "[ -d Build ] || mkdir Build"
    - "[ -d Build/RW ] || mkdir Build/RW"
    - "[ -d Build/RWExamples ] || mkdir Build/RWExamples"
    - "[ -d Build/RWHW ] || mkdir Build/RWHW"
    - "[ -d Build/RWS ] || mkdir Build/RWS"
    - "[ -d Build/RWSExamples ] || mkdir Build/RWSExamples"
    - "[ -d Build/RWSExamples/pluginapp ] || mkdir Build/RWSExamples/pluginapp"
    - "[ -d Build/RWSExamples/pluginUIapp ] || mkdir Build/RWSExamples/pluginUIapp"
    - "[ -d Build/RWSExamples/tutorial ] || mkdir Build/RWSExamples/tutorial"
    - "[ -d Build/RWSExamples/UDPKinPlugin ] || mkdir Build/RWSExamples/UDPKinPlugin"
    - "[ -d Build/RWSim ] || mkdir Build/RWSim"
    - "[ -d Build/RWSimExamples ] || mkdir Build/RWSimExamples"
    - "[ -d Build/RWHWExamples ] || mkdir Build/RWHWExamples"
    - "[ -d gtest ] || mkdir gtest"
  script:
    - cd Build
    # Build RobWork
    - cd RW
    - *is_shared_runner
    - "[[ $SHARED_RUNNER ]] || cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_sdurw_java=ON -DBUILD_sdurw_python=ON -DBUILD_sdurw_lua=ON ../../RobWork"
    - "[[ $SHARED_RUNNER ]] && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_DISABLE_FIND_PACKAGE_XercesC=True -DCMAKE_DISABLE_FIND_PACKAGE_SWIG=True -DRW_USE_FCL=OFF -DRW_USE_ASSIMP=OFF ../../RobWork"
    - make -j${THREADS}
  artifacts:
    paths:
      - ./Build
      - ./RobWork
      - ./gtest
    expire_in: 2 day
    when: on_success

.linux-build-RWS:
  extends: .CI-common-linux
  stage: build_second
  tags:
    - linux
  script:
    - cd Build
    # Build RobWorkStudio
    - cd RWS
    - *is_shared_runner
    - "[[ $SHARED_RUNNER ]] || cmake -DCMAKE_BUILD_TYPE=Release -DUSE_WERROR=$WERROR ../../RobWorkStudio"
    - "[[ $SHARED_RUNNER ]] && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_DISABLE_FIND_PACKAGE_SWIG=True ../../RobWorkStudio"
    - make -j${THREADS}
    - cd ..
  artifacts:
    paths:
      - ./Build/RWS
      - ./RobWorkStudio
    expire_in: 2 day
    when: on_success

.linux-build-RWHW:
  extends: .CI-common-linux
  stage: build_second
  tags:
    - linux
  script:
    - cd Build
    # Build RobWorkHardware
    - cd RWHW
    - cmake -DCMAKE_BUILD_TYPE=Release -DUSE_WERROR=$WERROR ../../RobWorkHardware
    - make -j${THREADS}

  artifacts:
    paths:
      - ./Build/RWHW
      - ./RobWorkHardware
    expire_in: 2 day
    when: on_success

.linux-build-RWSim:
  extends: .CI-common-linux
  stage: build_third
  tags:
    - linux
  script:
    - cd Build
    # Build RobWorkSim
    - cd RWSim
    - *is_shared_runner
    - "[[ $SHARED_RUNNER ]] || cmake -DCMAKE_BUILD_TYPE=Release -DUSE_WERROR=$WERROR ../../RobWorkSim"
    - "[[ $SHARED_RUNNER ]] && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_DISABLE_FIND_PACKAGE_SWIG=True ../../RobWorkSim"
    - make -j${THREADS}
    - cd ..
  artifacts:
    paths:
      - ./Build/RWSim
      - ./RobWorkSim
    expire_in: 2 day
    when: on_success

.linux-examples:
  extends: .CI-common-linux
  stage: test
  tags:
    - linux
  script:
    - cd Build
    # Build RobWork Examples
    - cd RWExamples
    - cmake -DCMAKE_BUILD_TYPE=Release ../../RobWork/example
    - make -j${THREADS}
    - cd ..
    # Build RobWorkStudio Examples
    - cd RWSExamples
    - cd pluginapp
    - cmake -DCMAKE_BUILD_TYPE=Release ../../../RobWorkStudio/example/pluginapp
    - make -j${THREADS}
    - cd ..
    - cd pluginUIapp
    - cmake -DCMAKE_BUILD_TYPE=Release ../../../RobWorkStudio/example/pluginUIapp
    - make -j${THREADS}
    - cd ..
    - cd tutorial
    - cmake -DCMAKE_BUILD_TYPE=Release ../../../RobWorkStudio/example/tutorial
    - make -j${THREADS}
    - cd ..
    - cd UDPKinPlugin
    - cmake -DCMAKE_BUILD_TYPE=Release ../../../RobWorkStudio/example/UDPKinPlugin
    - make -j${THREADS}
    - cd ..
    - cd ..
    # Build RobWorkSim Examples
    - cd RWSimExamples
    - cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkSim/example
    - make -j$(THREADS)
    - cd ..
    # Build RobWorkHardware Examples
    - cd RWHWExamples
    - cmake -DCMAKE_BUILD_TYPE=Release ../../RobWorkHardware/example
    - make -j$(THREADS)
    
.linux-tests:
  extends: .CI-common-linux
  stage: test
  tags:
    - linux
  script:
    - cd Build
    # Run RobWork tests
    - cd RW
    - make -j 1 -k sdurw-gtest_reports
    - mv ../../RobWork/bin/**/gtest_reports/* ../../gtest/
    - ctest --no-compress-output -T Test -j 1
    - mv ./Testing/**/Test.xml ../../RWS-Test.xml
    - cd ..
    # Run RobWorkStudio tests
    - cd RWS
    - export DISPLAY=:0
    - Xvfb $DISPLAY -screen 0 640x480x24 &
    - make -j 1 -k sdurws-gtest_reports
    - mv ../../RobWorkStudio/bin/**/gtest_reports/* ../../gtest/
    - ctest --no-compress-output -T Test -j 1
    - mv ./Testing/**/Test.xml ../../RWS-CTest.xml
    - cd ..
    # Run RobWorkSim tests
    - cd RWSim
    - make -j 1 -k sdurwsim-gtest_reports
    - mv ../../RobWorkSim/bin/**/gtest_reports/* ../../gtest/
    - ctest --no-compress-output -T Test -j 1 || ctest --no-compress-output -T Test
    - mv ./Testing/**/Test.xml ../../RWSim-CTest.xml

.linux-install-tests:
  extends: .CI-common-linux
  stage: test
  tags:
    - linux
  before_script:
    - *remove-rw-install
    - cd ./Build/RW
    - make install
    - cd ../RWHW
    - make install
    - cd ../RWS
    - make install
    - cd ../RWSim
    - make install
    - cd ../..
    - export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib
    - git clean -fdX
  script:
    - DIR=$(pwd)
    # TEST RW CPP FILES
    - cd $DIR/RobWork/example/cpp
    - mkdir Build && cd Build
    - cmake ..
    - make -j$(nproc)
    - "for file in ./ex-*; do if [ -f $file ] && [[ ! $file =~ ex-load-workcell ]] ; then  echo -e '\n EXECUTING: '$file && $file /home/robworkdata && EXERW=$EXERW' '$file || exit 1 ; fi; done"
    - ./ex-load-workcell ../../ModelData/XMLScenes/RobotWithHand/Workcell.xml
    # TEST RW PYTHON FILES
    - *is_shared_runner
    - cd $DIR/RobWork/example/python
    - "[[ $SHARED_RUNNER ]] || for file in $EXERW ; do echo -e '\n EXECUTING: '$file && python3 ${file}.py /home/robworkdata || exit 1 ; done"
    - "[[ $SHARED_RUNNER ]] || python3 ./ex-load-workcell.py ../ModelData/XMLScenes/RobotWithHand/Workcell.xml"
    # TEST RW LUA FILES
    - cd $DIR/RobWork/example/lua
    - "[[ $SHARED_RUNNER ]] || for file in $EXERW ; do echo -e '\n EXECUTING: '$file && /usr/bin/lua[0-9]* ${file}.lua /home/robworkdata || exit 1 ; done"
    - "[[ $SHARED_RUNNER ]] || /usr/bin/lua[0-9]* ./ex-load-workcell.lua ../ModelData/XMLScenes/RobotWithHand/Workcell.xml"
    # Test RWS CPP FILES
    - cd $DIR/RobWorkStudio/example/cpp
    - mkdir build && cd build
    - cmake ..
    - export DISPLAY=:0
    - Xvfb $DISPLAY -screen 0 640x480x24 &
    - make -j$(nproc)
    - "for file in ./ex-*; do if [ -f $file ] ; then  echo -e '\n EXECUTING: '$file && $file /home/robworkdata -t && EXERWS=$EXERWS' '$file || exit 1 ; fi; done"
    # Test RWS PYTHON FILES
    - cd $DIR/RobWorkStudio/example/python
    - "[[ $SHARED_RUNNER ]] || for file in $EXERWS ; do echo -e '\n EXECUTING: '$file && python3 ${file}.py /home/robworkdata -t || exit 1 ; done"
    # Test RWS LUA FILES
    - cd $DIR/RobWorkStudio/example/lua
    - "[[ $SHARED_RUNNER ]] || for file in $EXERWS ; do echo -e '\n EXECUTING: '$file && /usr/bin/lua[0-9]* ${file}.lua /home/robworkdata -t || exit 1 ; done"
    # Test RWSIM CPP files
    - cd $DIR/RobWorkSim/example/cpp
    - mkdir build && cd build
    - cmake ..
    - make -j$(nproc)
    - "for file in ./ex-*; do if [ -f $file ] ; then  echo -e '\n EXECUTING: '$file && $file /home/robworkdata && EXERWSIM=$EXERWSIM' '$file || exit 1 ; fi; done"
    # Test RWSSIM PYTHON FILES
    - cd $DIR/RobWorkSim/example/python
    - "[[ $SHARED_RUNNER ]] || for file in $EXERWSIM ; do echo -e '\n EXECUTING: '$file && python3 ${file}.py /home/robworkdata || exit 1 ; done"
    # Test RWSIM LUA FILES
    - cd $DIR/RobWorkSim/example/lua
    - "[[ $SHARED_RUNNER ]] || for file in $EXERWSIM ; do echo -e '\n EXECUTING: '$file && /usr/bin/lua[0-9]* ${file}.lua /home/robworkdata || exit 1 ; done"
    # Test RWHW CPP files
    - cd $DIR/RobWorkHardware/example/cpp
    - mkdir build && cd build
    - cmake ..
    - make -j$(nproc)
    - "for file in ./ex-*; do if [ -f $file ] ; then  echo -e '\n EXECUTING: '$file && $file localhost && EXERWHW=$EXERWHW' '$file || exit 1 ; fi; done"

.linux-build-debian:
  extends: .CI-common-linux
  stage: build
  tags:
    - linux
  before_script:
    - *uninstall-rw-deb
    - cd ..
    - rm -r ./tmp || echo "nothing to remove" && mkdir tmp
    - cp -r ./RobWork ./tmp
    - cd tmp/RobWork
    - git checkout origin/debian-pkg
    - cd ../..
    - cp -r ./tmp/RobWork/debian ./RobWork
    - cd RobWork
    - rm -r ../*.deb || echo "Nothing to remove"
    - rm -r ../robwork_* || echo "Nothing to remove"
  script:
    - version=$(./debian/scripts/getVersion.sh ./debian/scripts -n -p1)
    - mv ./debian/changelog ./debian/changelog.old
    - echo "robwork ("$version"-1) bionic; urgency=medium" >> ./debian/changelog && echo "" >> ./debian/changelog
    - echo "  * Test" >> ./debian/changelog && echo "" >> ./debian/changelog
    - echo " -- GitLab CI Builder <test@test.test> " $(date "+%a, %d %b %Y %T +0100") >> ./debian/changelog
    - echo "" >> ./debian/changelog
    - cat ./debian/changelog.old >> ./debian/changelog
    - tar --exclude='./debian' --exclude='./.git' -zcvf ../robwork_$version.orig.tar.gz . > /dev/null
    - debuild --no-sign
    - mkdir DEBPKG
    - find ../ -maxdepth 1 -type f -name '*.deb' -exec cp '{}' ./DEBPKG ';'
  artifacts:
    paths:
      - ./DEBPKG
      - ./debian
    expire_in: 2 day
    when: on_success

.linux-test-debian:
  extends: .linux-install-tests
  stage: test
  tags:
    - linux
  before_script:
    - *uninstall-rw-deb
    - apt -y install ./DEBPKG/*.deb

########################### WINDOWS CI ################################

.windows-build-RW:
  extends: .CI-common
  stage: build
  tags:
    - windows
  before_script:
    - "if not exist Build mkdir Build"
    - "if not exist Build\\RW mkdir Build\\RW"
    - "if not exist Build\\RWExamples mkdir Build\\RWExamples"
    - "if not exist Build\\RWHW mkdir Build\\RWHW"
    - "if not exist Build\\RWS mkdir Build\\RWS"
    - "if not exist Build\\RWSExamples mkdir Build\\RWSExamples"
    - "if not exist Build\\RWSExamples\\pluginapp mkdir Build\\RWSExamples\\pluginapp"
    - "if not exist Build\\RWSExamples\\pluginUIapp mkdir Build\\RWSExamples\\pluginUIapp"
    - "if not exist Build\\RWSExamples\\tutorial mkdir Build\\RWSExamples\\tutorial"
    - "if not exist Build\\RWSExamples\\UDPKinPlugin mkdir Build\\RWSExamples\\UDPKinPlugin"
    - "if not exist Build\\RWSim mkdir Build\\RWSim"
    - "if not exist Build\\RWSimExamples mkdir Build\\RWSimExamples"
    - "if not exist gtest mkdir gtest"
  script:
    - cd Build
    # Build RobWork
    - cd RW
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" -DSWIG_EXECUTABLE=%SWIG_ROOT%\swig.exe -DBUILD_sdurw_java=ON -DBUILD_sdurw_python=ON -DBUILD_sdurw_lua=ON ../../RobWork
    - msbuild RobWork.sln /property:Configuration=Release /maxcpucount:8
  artifacts:
    paths:
      - ./Build
      - ./RobWork
      - ./gtest
    expire_in: 2 day
    when: on_success

.windows-build-RWHW:
  extends: .CI-common
  stage: build_second
  tags:
    - windows
  script:
    - cd Build
    # Build RobWorkHardware
    - cd RWHW
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" -DBUILD_SHARED_LIBS=OFF -DRWHW_SHARED_LIBS=OFF ../../RobWorkHardware
    - msbuild RobWorkHardware.sln /property:Configuration=Release /maxcpucount:8
  artifacts:
    paths:
      - ./Build/RWHW
      - ./RobWorkHardware
    expire_in: 2 day
    when: on_success

.windows-build-RWS:
  extends: .CI-common
  stage: build_second
  tags:
    - windows
  script:
    - cd Build
    # Build RobWorkStudio
    - cd RWS
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" -DSWIG_EXECUTABLE=%SWIG_ROOT%\swig.exe ../../RobWorkStudio
    - msbuild RobWorkStudio.sln /property:Configuration=Release /maxcpucount:6
  artifacts:
    paths:
      - ./Build/RWS
      - ./RobWorkStudio
    expire_in: 2 day
    when: on_success

.windows-build-RWSim:
  extends: .CI-common
  stage: build_third
  tags:
    - windows
  script:
    - cd Build
    - cd RWSim
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" -DSWIG_EXECUTABLE=%SWIG_ROOT%\swig.exe ../../RobWorkSim
    - msbuild RobWorkSim.sln /property:Configuration=Release /maxcpucount:8
  artifacts:
    paths:
      - ./Build/RWSim
      - ./RobWorkSim
    expire_in: 2 day
    when: on_success

.windows-examples:
  extends: .CI-common
  stage: test
  tags:
    - windows
  script:
    - cd Build
    # Build RobWork Examples
    - cd RWExamples
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../RobWork/example
    - msbuild Project.sln /property:Configuration=Release /maxcpucount:8
    - cd ..
    # Build RobWorkStudio Examples
    - cd RWSExamples
    - cd pluginapp
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../../RobWorkStudio/example/pluginapp
    - msbuild PluginApp.sln /property:Configuration=Release /maxcpucount:8
    - cd ..
    - cd pluginUIapp
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../../RobWorkStudio/example/pluginUIapp
    - msbuild PluginUIApp.sln /property:Configuration=Release /maxcpucount:8
    - cd ..
    - cd tutorial
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../../RobWorkStudio/example/tutorial
    - msbuild SamplePluginApp.sln /property:Configuration=Release /maxcpucount:8
    - cd ..
    - cd UDPKinPlugin
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../../RobWorkStudio/example/UDPKinPlugin
    - msbuild PluginUIApp.sln /property:Configuration=Release /maxcpucount:8
    - cd ..
    - cd ..
    # Build RobWorkSim Examples
    - cd RWSimExamples
    - cmake -DCMAKE_BUILD_TYPE=Release -G "Visual Studio 15 2017 Win64" ../../RobWorkSim/example
    - msbuild RWSimExamples.sln /property:Configuration=Release /maxcpucount:8

.windows-tests:
  extends: .CI-common
  stage: test
  tags:
    - windows
  script:
    - cd Build
    # Run RobWork tests
    - cd RW
    - msbuild gtest/sdurw-gtest_reports.vcxproj /p:Configuration=Release
    - move ..\\..\\RobWork\\bin\\release\\gtest_reports\\* ..\\..\\gtest\\
    - ctest -C Release --no-compress-output -T Test -j 1
    - FOR /D %%F IN ("Testing\\*") DO IF EXIST %%F\\Test.xml move %%F\\Test.xml ..\\..\\RW-CTest.xml
    - cd ..
    # Run RobWorkStudio tests
    - cd RWS
    - msbuild gtest/sdurws-gtest_reports.vcxproj /p:Configuration=Release
    - move ..\\..\\RobWorkStudio\\bin\\release\\gtest_reports\\* ..\\..\\gtest\\
    - ctest -C Release --no-compress-output -T Test -j 1
    - FOR /D %%F IN ("Testing\\*") DO IF EXIST %%F\\Test.xml move %%F\\Test.xml ..\\..\\RWS-CTest.xml
    - cd ..
    # Run RobWorkSim tests
    - cd RWSim
    - msbuild gtest/sdurwsim-gtest_reports.vcxproj /p:Configuration=Release
    - move ..\\..\\RobWorkSim\\bin\\Release\\gtest_reports\\* ..\\..\\gtest\\
    - ctest -C Release --no-compress-output -T Test -j 1
    - FOR /D %%F IN ("Testing\\*") DO IF EXIST %%F\\Test.xml move %%F\\Test.xml ..\\..\\RWSim-CTest.xml


############################# MacOs CI #################################

.mac-build-RW:
  extends: .linux-build-RW
  before_script:
    - THREADS=3
    - "[ -d Build ] || mkdir Build"
    - "[ -d Build/RW ] || mkdir Build/RW"
    - "[ -d Build/RWExamples ] || mkdir Build/RWExamples"
    - "[ -d Build/RWHW ] || mkdir Build/RWHW"
    - "[ -d Build/RWS ] || mkdir Build/RWS"
    - "[ -d Build/RWSExamples ] || mkdir Build/RWSExamples"
    - "[ -d Build/RWSExamples/pluginapp ] || mkdir Build/RWSExamples/pluginapp"
    - "[ -d Build/RWSExamples/pluginUIapp ] || mkdir Build/RWSExamples/pluginUIapp"
    - "[ -d Build/RWSExamples/tutorial ] || mkdir Build/RWSExamples/tutorial"
    - "[ -d Build/RWSExamples/UDPKinPlugin ] || mkdir Build/RWSExamples/UDPKinPlugin"
    - "[ -d Build/RWSim ] || mkdir Build/RWSim"
    - "[ -d Build/RWSimExamples ] || mkdir Build/RWSimExamples"
    - "[ -d gtest ] || mkdir gtest"
  tags:
    - mac

.mac-build-RWS:
  extends: .linux-build-RWS
  before_script:
    - THREADS=3
    - WERROR=False
  tags:
    - mac

.mac-build-RWSim:
  extends: .linux-build-RWSim
  before_script:
    - THREADS=3
    - WERROR=False
  tags:
    - mac

.mac-build-RWHW:
  extends: .linux-build-RWHW
  before_script:
    - THREADS=3
    - WERROR=False
  tags:
    - mac
  
.mac-examples:
  extends: .linux-examples
  before_script:
    - THREADS=3
    - WERROR=False
  tags:
    - mac


############################# Mac High-sierra ###########################

build-RW:macOS-HighSierra:
  extends: .mac-build-RW

build-RWS:macOS-HighSierra:
  extends: .mac-build-RWS
  dependencies:
    - build-RW:macOS-HighSierra
  needs: ["build-RW:macOS-HighSierra"]

build-RWHW:macOS-HighSierra:
  extends: .mac-build-RWHW
  dependencies:
    - build-RW:macOS-HighSierra
  needs: ["build-RW:macOS-HighSierra"]

build-RWSim:macOS-HighSierra:
  extends: .mac-build-RWSim
  dependencies:
    - build-RW:macOS-HighSierra
    - build-RWS:macOS-HighSierra
  needs: ["build-RW:macOS-HighSierra", "build-RWS:macOS-HighSierra"]

########################## WINDOWS vs15 ################################

build-RW:windows10-vs15:
  extends: .windows-build-RW

build-RWS:windows10-vs15:
  extends: .windows-build-RWS
  dependencies:
    - build-RW:windows10-vs15
  needs: ["build-RW:windows10-vs15"]

build-RWHW:windows10-vs15:
  extends: .windows-build-RWHW
  dependencies:
    - build-RW:windows10-vs15
  needs: ["build-RW:windows10-vs15"]

build-RWSim:windows10-vs15:
  extends: .windows-build-RWSim
  dependencies:
    - build-RW:windows10-vs15
    - build-RWS:windows10-vs15
  needs: ["build-RW:windows10-vs15", "build-RWS:windows10-vs15"]

examples:windows10-vs15:
  extends: .windows-examples
  dependencies:
    - build-RW:windows10-vs15
    - build-RWS:windows10-vs15
    - build-RWHW:windows10-vs15
    - build-RWSim:windows10-vs15
  needs:
    [
      "build-RW:windows10-vs15",
      "build-RWS:windows10-vs15",
      "build-RWHW:windows10-vs15",
      "build-RWSim:windows10-vs15",
    ]

tests:windows10-vs15:
  extends: .windows-tests
  dependencies:
    - build-RW:windows10-vs15
    - build-RWS:windows10-vs15
    - build-RWHW:windows10-vs15
    - build-RWSim:windows10-vs15
  needs:
    [
      "build-RW:windows10-vs15",
      "build-RWS:windows10-vs15",
      "build-RWHW:windows10-vs15",
      "build-RWSim:windows10-vs15",
    ]

########################## Ubuntu 1804 (MAIN) ################################

build-RW:ubuntu-1804:
  extends: .linux-build-RW
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

build-RWS:ubuntu-1804:
  extends: .linux-build-RWS
  dependencies:
    - build-RW:ubuntu-1804
  needs: ["build-RW:ubuntu-1804"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

build-RWHW:ubuntu-1804:
  extends: .linux-build-RWHW
  dependencies:
    - build-RW:ubuntu-1804
  needs: ["build-RW:ubuntu-1804"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

build-RWSim:ubuntu-1804:
  extends: .linux-build-RWSim
  dependencies:
    - build-RW:ubuntu-1804
    - build-RWS:ubuntu-1804
  needs: ["build-RW:ubuntu-1804", "build-RWS:ubuntu-1804"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

examples:ubuntu-1804:
  extends: .linux-examples
  dependencies:
    - build-RW:ubuntu-1804
    - build-RWS:ubuntu-1804
    - build-RWHW:ubuntu-1804
    - build-RWSim:ubuntu-1804
  needs:
    [
      "build-RW:ubuntu-1804",
      "build-RWS:ubuntu-1804",
      "build-RWHW:ubuntu-1804",
      "build-RWSim:ubuntu-1804",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

tests:ubuntu-1804:
  extends: .linux-tests
  dependencies:
    - build-RW:ubuntu-1804
    - build-RWS:ubuntu-1804
    - build-RWHW:ubuntu-1804
    - build-RWSim:ubuntu-1804
  needs:
    [
      "build-RW:ubuntu-1804",
      "build-RWS:ubuntu-1804",
      "build-RWHW:ubuntu-1804",
      "build-RWSim:ubuntu-1804",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

install-tests:ubuntu-1804:
  extends: .linux-install-tests
  dependencies:
    - build-RW:ubuntu-1804
    - build-RWS:ubuntu-1804
    - build-RWHW:ubuntu-1804
    - build-RWSim:ubuntu-1804
  needs:
    [
      "build-RW:ubuntu-1804",
      "build-RWS:ubuntu-1804",
      "build-RWHW:ubuntu-1804",
      "build-RWSim:ubuntu-1804",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804
  only:
    refs:
      - branches@sdurobotics/RobWork
      - tags@sdurobotics/RobWork
      - schedules
      - web
      - merge_requests
      - /^ci-.*$/
  except:

build-debian-pre:ubuntu-1804:
  extends: .linux-build-debian
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-deb

test-debian-pre:ubuntu-1804:
  extends: .linux-test-debian
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-deb
  dependencies:
    - "build-debian-pre:ubuntu-1804"
  needs: ["build-debian-pre:ubuntu-1804"]

########################## Ubuntu 1604 ################################

build-RW:ubuntu-1604:
  extends: .linux-build-RW
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

build-RWS:ubuntu-1604:
  extends: .linux-build-RWS
  dependencies:
    - build-RW:ubuntu-1604
  needs: ["build-RW:ubuntu-1604"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

build-RWHW:ubuntu-1604:
  extends: .linux-build-RWHW
  dependencies:
    - build-RW:ubuntu-1604
  needs: ["build-RW:ubuntu-1604"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

build-RWSim:ubuntu-1604:
  extends: .linux-build-RWSim
  dependencies:
    - build-RW:ubuntu-1604
    - build-RWS:ubuntu-1604
  needs: ["build-RW:ubuntu-1604", "build-RWS:ubuntu-1604"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

examples:ubuntu-1604:
  extends: .linux-examples
  dependencies:
    - build-RW:ubuntu-1604
    - build-RWS:ubuntu-1604
    - build-RWHW:ubuntu-1604
    - build-RWSim:ubuntu-1604
  needs:
    [
      "build-RW:ubuntu-1604",
      "build-RWS:ubuntu-1604",
      "build-RWHW:ubuntu-1604",
      "build-RWSim:ubuntu-1604",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

tests:ubuntu-1604:
  extends: .linux-tests
  dependencies:
    - build-RW:ubuntu-1604
    - build-RWS:ubuntu-1604
    - build-RWHW:ubuntu-1604
    - build-RWSim:ubuntu-1604
  needs:
    [
      "build-RW:ubuntu-1604",
      "build-RWS:ubuntu-1604",
      "build-RWHW:ubuntu-1604",
      "build-RWSim:ubuntu-1604",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1604

########################## Ubuntu 1910 ################################
build-RW:ubuntu-1910:
  extends: .linux-build-RW
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

build-RWS:ubuntu-1910:
  extends: .linux-build-RWS
  dependencies:
    - build-RW:ubuntu-1910
  needs: ["build-RW:ubuntu-1910"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

build-RWHW:ubuntu-1910:
  extends: .linux-build-RWHW
  dependencies:
    - build-RW:ubuntu-1910
  needs: ["build-RW:ubuntu-1910"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

build-RWSim:ubuntu-1910:
  extends: .linux-build-RWSim
  dependencies:
    - build-RW:ubuntu-1910
    - build-RWS:ubuntu-1910
  needs: ["build-RW:ubuntu-1910", "build-RWS:ubuntu-1910"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

examples:ubuntu-1910:
  extends: .linux-examples
  dependencies:
    - build-RW:ubuntu-1910
    - build-RWS:ubuntu-1910
    - build-RWHW:ubuntu-1910
    - build-RWSim:ubuntu-1910
  needs:
    [
      "build-RW:ubuntu-1910",
      "build-RWS:ubuntu-1910",
      "build-RWHW:ubuntu-1910",
      "build-RWSim:ubuntu-1910",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

tests:ubuntu-1910:
  extends: .linux-tests
  dependencies:
    - build-RW:ubuntu-1910
    - build-RWS:ubuntu-1910
    - build-RWHW:ubuntu-1910
    - build-RWSim:ubuntu-1910
  needs:
    [
      "build-RW:ubuntu-1910",
      "build-RWS:ubuntu-1910",
      "build-RWHW:ubuntu-1910",
      "build-RWSim:ubuntu-1910",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1910

########################## Ubuntu 2004 ################################
build-RW:ubuntu-2004:
  extends: .linux-build-RW
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

build-RWS:ubuntu-2004:
  extends: .linux-build-RWS
  dependencies:
    - build-RW:ubuntu-2004
  needs: ["build-RW:ubuntu-2004"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

build-RWHW:ubuntu-2004:
  extends: .linux-build-RWHW
  dependencies:
    - build-RW:ubuntu-2004
  needs: ["build-RW:ubuntu-2004"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

build-RWSim:ubuntu-2004:
  extends: .linux-build-RWSim
  dependencies:
    - build-RW:ubuntu-2004
    - build-RWS:ubuntu-2004
  needs: ["build-RW:ubuntu-2004", "build-RWS:ubuntu-2004"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

examples:ubuntu-2004:
  extends: .linux-examples
  dependencies:
    - build-RW:ubuntu-2004
    - build-RWS:ubuntu-2004
    - build-RWHW:ubuntu-2004
    - build-RWSim:ubuntu-2004
  needs:
    [
      "build-RW:ubuntu-2004",
      "build-RWS:ubuntu-2004",
      "build-RWHW:ubuntu-2004",
      "build-RWSim:ubuntu-2004",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

tests:ubuntu-2004:
  extends: .linux-tests
  dependencies:
    - build-RW:ubuntu-2004
    - build-RWS:ubuntu-2004
    - build-RWHW:ubuntu-2004
    - build-RWSim:ubuntu-2004
  needs:
    [
      "build-RW:ubuntu-2004",
      "build-RWS:ubuntu-2004",
      "build-RWHW:ubuntu-2004",
      "build-RWSim:ubuntu-2004",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

install-tests:ubuntu-2004:
  extends: .linux-install-tests
  dependencies:
    - build-RW:ubuntu-2004
    - build-RWS:ubuntu-2004
    - build-RWHW:ubuntu-2004
    - build-RWSim:ubuntu-2004
  needs:
    [
      "build-RW:ubuntu-2004",
      "build-RWS:ubuntu-2004",
      "build-RWHW:ubuntu-2004",
      "build-RWSim:ubuntu-2004",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004

build-debian-pre:ubuntu-2004:
  extends: .linux-build-debian
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-deb

test-debian-pre:ubuntu-2004:
  extends: .linux-test-debian
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-deb
  dependencies:
    - "build-debian-pre:ubuntu-2004"
  needs: ["build-debian-pre:ubuntu-2004"]


########################## Centos 7 ################################
build-RW:centos-7:
  extends: .linux-build-RW
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

build-RWS:centos-7:
  extends: .linux-build-RWS
  dependencies:
    - build-RW:centos-7
  needs: ["build-RW:centos-7"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

build-RWHW:centos-7:
  extends: .linux-build-RWHW
  dependencies:
    - build-RW:centos-7
  needs: ["build-RW:centos-7"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

build-RWSim:centos-7:
  extends: .linux-build-RWSim
  dependencies:
    - build-RW:centos-7
    - build-RWS:centos-7
  needs: ["build-RW:centos-7", "build-RWS:centos-7"]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

examples:centos-7:
  extends: .linux-examples
  dependencies:
    - build-RW:centos-7
    - build-RWS:centos-7
    - build-RWHW:centos-7
    - build-RWSim:centos-7
  needs:
    [
      "build-RW:centos-7",
      "build-RWS:centos-7",
      "build-RWHW:centos-7",
      "build-RWSim:centos-7",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

tests:centos-7:
  extends: .linux-tests
  dependencies:
    - build-RW:centos-7
    - build-RWS:centos-7
    - build-RWHW:centos-7
    - build-RWSim:centos-7
  needs:
    [
      "build-RW:centos-7",
      "build-RWS:centos-7",
      "build-RWHW:centos-7",
      "build-RWSim:centos-7",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:centos-7

######################## Misc ##########################################

build:doc:
  extends: .linux-doc
  dependencies:
    - build-RW:ubuntu-1804
    - build-RWS:ubuntu-1804
    - build-RWHW:ubuntu-1804
    - build-RWSim:ubuntu-1804
  needs:
    [
      "build-RW:ubuntu-1804",
      "build-RWS:ubuntu-1804",
      "build-RWHW:ubuntu-1804",
      "build-RWSim:ubuntu-1804",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-doc

build:doc-2004:
  extends: .linux-doc
  dependencies:
    - build-RW:ubuntu-2004
    - build-RWS:ubuntu-2004
    - build-RWHW:ubuntu-2004
    - build-RWSim:ubuntu-2004
  needs:
    [
      "build-RW:ubuntu-2004",
      "build-RWS:ubuntu-2004",
      "build-RWHW:ubuntu-2004",
      "build-RWSim:ubuntu-2004",
    ]
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-doc

pages:
  stage: deploy
  tags:
    - linux
  only:
    refs:
      - master@sdurobotics/RobWork
      - schedules
      - web
  variables:
    GIT_STRATEGY: none
  script:
    - echo "Deploying documentation."
  dependencies:
    - build:doc
  needs: ["build:doc"]
  artifacts:
    paths:
      - public
    expire_in: 1 week

######################## debian package ##########################################

.debian-build:
  extends: .linux-build-debian
  stage: build
  tags:
    - linux
  before_script:
   - rm -r ../*.deb || echo nothing to remove
  only:
    refs:
      - debian-pkg@sdurobotics/RobWork
      - schedules
      - web
  except:
    refs:
      - /^wip-.*$/
    variables:
      - $CI_COMMIT_MESSAGE =~ /^WIP:/

.debian-test:
  extends: .linux-test-debian
  stage: test
  tags:
    - linux
  only:
    refs:
      - debian-pkg@sdurobotics/RobWork
      - schedules
      - web
  except:
    refs:
      - /^wip-.*$/
    variables:
      - $CI_COMMIT_MESSAGE =~ /^WIP:/

.debian-upgrade-test:
  stage: test
  tags:
    - linux
  only:
    refs:
      - debian-pkg@sdurobotics/RobWork
      - schedules
      - web
  except:
    refs:
      - /^wip-.*$/
    variables:
      - $CI_COMMIT_MESSAGE =~ /^WIP:/
  before_script:
    - *uninstall-rw-deb
    - add-apt-repository ppa:sdurobotics/robwork
    - apt-get update
  script:
    - apt -y install libsdurw-all-dev libsdurws-all-dev libsdurwhw-all-dev libsdurwsim-all-dev
    - apt -y install ./DEBPKG/*.deb  

################# debian-pkg ubuntu-1804 #################

build-debian:ubuntu-1804:
  extends: .debian-build
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-deb

test-debian:ubuntu-1804:
  extends: .debian-test
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-deb
  dependencies:
    - build-debian:ubuntu-1804
  needs: ["build-debian:ubuntu-1804"]

test-debian-upgrade:ubuntu-1804:
  extends: .debian-upgrade-test
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-1804-deb
  dependencies:
    - build-debian:ubuntu-1804
  needs: ["build-debian:ubuntu-1804"]

########################## Debian pkg ubuntu 2004 ######################################

build-debian:ubuntu-2004:
  extends: .debian-build
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-deb

test-debian:ubuntu-2004:
  extends: .debian-test
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-deb
  dependencies:
    - build-debian:ubuntu-2004
  needs: ["build-debian:ubuntu-2004"]

test-debian-upgrade:ubuntu-2004:
  extends: .debian-upgrade-test
  image: registry.gitlab.com/sdurobotics/docker-images/robwork-build:ubuntu-2004-deb
  dependencies:
    - build-debian:ubuntu-2004
  needs: ["build-debian:ubuntu-2004"]
  
